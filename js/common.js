var titleTextArr = [];
var urlTextArr = [];
var imgUrlArr = [];
var optionArr = [];


$(document).ready(function() {

    $("#insertCanvas").click(function() {
        $.ajax({
            url: "https://jsonplaceholder.typicode.com/photos",
            success: function(result) {

                // first two and last two object
                var firstObj = 0;
                var secondObj = 1;
                var lastObj = result.length - 1;
                var secondLastObj = result.length - 2;

                // random object
                var randomObj = Math.floor(Math.random() * (4998 - 2)) + 2;

                // insert objects in canvas as per requirements //

                // if ‘id’ is even, display ‘title’ in the selected canvas
                var titleText = result[secondObj].title;
                titleTextArr = [...titleTextArr, titleText];

                // if ‘id’ is odd, insert ‘thumbnailUrl’ as an image in the selected canvas. 
                var thumbnailUrlImg = result[firstObj].thumbnailUrl;
                imgUrlArr = [...imgUrlArr, thumbnailUrlImg];

                // if ‘albumId’ >= 100, then display ‘url’ as text in the selected canvas
                var urlasText_1 = result[lastObj].url;
                var urlasText_2 = result[secondLastObj].url;
                urlTextArr = [...urlTextArr, urlasText_1, urlasText_2];

                if (result[randomObj].id % 2 == 0 && result[randomObj].id < 100) {
                    var title_randomObj = result[randomObj].title;
                    titleTextArr = [...titleTextArr, title_randomObj];
                } else if (result[randomObj].id % 2 != 0 && result[randomObj].id < 100) {
                    var thumbnailUrl_randomObj = result[randomObj].thumbnailUrl;
                    imgUrlArr = [...imgUrlArr, thumbnailUrl_randomObj];
                } else {
                    var Url_randomObj = result[randomObj].url;
                    urlTextArr = [...urlTextArr, Url_randomObj];
                }

                insertInCanvas();
            }
        });
    })


    // for creating 2 to 4 canvas dynamically 
    $("#createCanvas").click(function() {
        // re-setting the values of containers
        $("#canvas-container").html('');
        $("#selectCanvas").html('');

        var maxVal = Math.floor(Math.random() * (5 - 2)) + 2;

        // to create random canvas between 2 to 4
        for (let i = 0; i < maxVal; i++) {
            var canvas = document.createElement('canvas');
            canvas.id = "canvasArea" + (i + 1);
            canvas.className = "canvas-area";
            canvas.width = 280;
            canvas.height = 280

            $("#canvas-container").append(canvas);

            // insert options into select box
            var option = document.createElement("option");
            option.text = "canvasArea" + (i + 1);
            $("#selectCanvas").append(option);

            // make the select section visible
            $("#canvasInput").css('visibility', 'visible');
        }
        // just a information text regarding how many canvas we have
        $("#infoText").html("You have " + maxVal + " number of canvas available");
    });


});

function insertInCanvas() {
    var canvasSelected = $("#selectCanvas").val();

    $("#selectCanvas > option").each(function() {
        if (this.text != canvasSelected) {
            optionArr = [...optionArr, this.text];
        }
    });


    canvas = new fabric.Canvas(canvasSelected);
    var vCanvas = new fabric.Canvas();
    var myObj = new Array();

    // adding title in the canvas
    for (let i = 0; i < titleTextArr.length; i++) {
        myObj[i] = {
            objects: [{
                type: "text",
                originX: "center",
                originY: "center",
                left: 140,
                top: 20,
                width: 100,
                height: 30,
                fill: "#222f3e",
                overlayFill: null,
                stroke: null,
                strokeWidth: 1,
                strokeDashArray: null,
                scaleX: 1,
                scaleY: 1,
                angle: 0,
                flipX: false,
                flipY: false,
                opacity: 1,
                selectable: true,
                hasControls: true,
                hasBorders: true,
                hasRotatingPoint: true,
                transparentCorners: true,
                perPixelTargetFind: false,
                shadow: null,
                visible: true,
                text: titleTextArr[i],
                fontSize: 12,
                fontWeight: "normal",
                fontFamily: "Arial",
                fontStyle: "",
                lineHeight: 1.3,
                textDecoration: "",
                textShadow: "",
                textAlign: "left",
                path: null,
                strokeStyle: "",
                backgroundColor: "",
                textBackgroundColor: "",
                useNative: true,
            }]
        };

        vCanvas.loadFromDatalessJSON(myObj[i]);
        vCanvas.forEachObject(function(obj) {
            canvas.add(obj)
        });

    }

    // adding url as a text in canvas
    var topVal = 20;
    var leftVal = 140
    for (let i = 0; i < urlTextArr.length; i++) {
        topVal = topVal + 25;
        myObj[i] = {
            objects: [{
                type: "text",
                originX: "center",
                originY: "center",
                left: leftVal,
                top: topVal,
                width: 100,
                height: 30,
                fill: "#0652DD",
                overlayFill: null,
                stroke: null,
                strokeWidth: 1,
                strokeDashArray: null,
                scaleX: 1,
                scaleY: 1,
                angle: 0,
                flipX: false,
                flipY: false,
                opacity: 1,
                selectable: true,
                hasControls: true,
                hasBorders: true,
                hasRotatingPoint: true,
                transparentCorners: true,
                perPixelTargetFind: true,
                shadow: null,
                visible: true,
                text: urlTextArr[i],
                fontSize: 11,
                fontWeight: "normal",
                fontFamily: "Arial",
                fontStyle: "",
                lineHeight: 1.3,
                textDecoration: "",
                textShadow: "",
                textAlign: "left",
                path: null,
                strokeStyle: "",
                backgroundColor: "",
                textBackgroundColor: "",
                useNative: true,
            }]
        };

        vCanvas.loadFromDatalessJSON(myObj[i]);
        vCanvas.forEachObject(function(obj) {
            canvas.add(obj)
        });

    }

    // adding thumbnail url as image
    for (let i = 0; i < imgUrlArr.length; i++) {
        fabric.Image.fromURL(imgUrlArr[i], function(myImg) {
            myImg.top = 120;
            myImg.left = 60;
            canvas.add(myImg)
        });
    }

    canvas.renderAll();

    // clear the arrays
    titleTextArr = [];
    urlTextArr = [];
    imgUrlArr = [];
    setTimeout(function() {
        dragdropItems();
    }, 800)


}

// get ready to handle touch events
var isTouchDevice = false;
window.addEventListener("touchstart", function() {
    isTouchDevice = true;
});


function dragdropItems() {

    var migrateItem = function(fromCanvas, toCanvas, pendingImage) {
        fromCanvas.remove(pendingImage);

        var pendingTransform = fromCanvas._currentTransform;
        fromCanvas._currentTransform = null;

        var removeListener = fabric.util.removeListener;
        var addListener = fabric.util.addListener; {
            removeListener(fabric.document, 'mouseup', fromCanvas._onMouseUp);
            removeListener(fabric.document, 'touchend', fromCanvas._onMouseUp);

            removeListener(fabric.document, 'mousemove', fromCanvas._onMouseMove);
            removeListener(fabric.document, 'touchmove', fromCanvas._onMouseMove);

            addListener(fromCanvas.upperCanvasEl, 'mousemove', fromCanvas._onMouseMove);
            addListener(fromCanvas.upperCanvasEl, 'touchmove', fromCanvas._onMouseMove, {
                passive: false
            });

            if (isTouchDevice) {
                // Wait 500ms before rebinding mousedown to prevent double triggers
                // from touch devices
                var _this = fromCanvas;
                setTimeout(function() {
                    addListener(_this.upperCanvasEl, 'mousedown', _this._onMouseDown);
                }, 500);
            }
        } {
            addListener(fabric.document, 'touchend', toCanvas._onMouseUp, {
                passive: false
            });
            addListener(fabric.document, 'touchmove', toCanvas._onMouseMove, {
                passive: false
            });

            removeListener(toCanvas.upperCanvasEl, 'mousemove', toCanvas._onMouseMove);
            removeListener(toCanvas.upperCanvasEl, 'touchmove', toCanvas._onMouseMove);

            if (isTouchDevice) {
                // Unbind mousedown to prevent double triggers from touch devices
                removeListener(toCanvas.upperCanvasEl, 'mousedown', toCanvas._onMouseDown);
            } else {
                addListener(fabric.document, 'mouseup', toCanvas._onMouseUp);
                addListener(fabric.document, 'mousemove', toCanvas._onMouseMove);
            }
        }

        setTimeout(function() {
            pendingImage.scaleX *= 1;
            pendingImage.canvas = toCanvas;
            pendingImage.migrated = true;
            toCanvas.add(pendingImage);

            toCanvas._currentTransform = pendingTransform;
            toCanvas._currentTransform.scaleX *= -1;
            toCanvas._currentTransform.original.scaleX *= -1;
            toCanvas.setActiveObject(pendingImage);
        }, 10);
    };

    var onObjectMovingCase1 = function(p) {
        var viewport = p.target.canvas.calcViewportBoundaries();

        // canvas 1 to canvas 2
        if (p.target.canvas === canvas) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas, canvas2, p.target);
                return;
            }
        }

        // canvas 2 to canvas 1
        if (p.target.canvas === canvas2) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas2, canvas, p.target);
                return;
            }
        }


        // canvas 2 to canvas 3
        if (p.target.canvas === canvas2) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas2, canvas3, p.target);
                return;
            }
        }

        // canvas 3 to canvas 2
        if (p.target.canvas === canvas3) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas3, canvas2, p.target);
                return;
            }
        }

        // canvas 3 to canvas 4
        if (p.target.canvas === canvas3) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas3, canvas4, p.target);
                return;
            }
        }

        // canvas 4 to canvas 3
        if (p.target.canvas === canvas4) {
            if (p.target.left < viewport.tl.x) {
                console.log("Migrate: center -> left");
                migrateItem(canvas4, canvas3, p.target);
                return;
            }
        }

        // canvas 1 to canvas 4
        if (p.target.canvas === canvas) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas, canvas4, p.target);
                return;
            }
        }

        // canvas 4 to canvas 1
        if (p.target.canvas === canvas4) {
            if (p.target.left < viewport.tl.x) {
                console.log("Migrate: center -> left");
                migrateItem(canvas4, canvas, p.target);
                return;
            }
        }
    };

    var onObjectMovingCase2 = function(p) {
        var viewport = p.target.canvas.calcViewportBoundaries();

        // canvas 1 to canvas 2
        if (p.target.canvas === canvas1) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas1, canvas, p.target);
                return;
            }
        }

        // canvas 2 to canvas 1
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas1, p.target);
                return;
            }
        }


        // canvas 2 to canvas 3
        if (p.target.canvas === canvas) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas, canvas3, p.target);
                return;
            }
        }

        // canvas 3 to canvas 2
        if (p.target.canvas === canvas3) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas3, canvas, p.target);
                return;
            }
        }

        // canvas 2 to canvas 4
        if (p.target.canvas === canvas) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas, canvas4, p.target);
                return;
            }
        }

        // canvas 4 to canvas 2
        if (p.target.canvas === canvas4) {
            if (p.target.left < viewport.tl.x) {
                console.log("Migrate: center -> left");
                migrateItem(canvas4, canvas, p.target);
                return;
            }
        }

        // canvas 3 to canvas 4
        if (p.target.canvas === canvas3) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas3, canvas4, p.target);
                return;
            }
        }

        // canvas 4 to canvas 3
        if (p.target.canvas === canvas4) {
            if (p.target.left < viewport.tl.x) {
                console.log("Migrate: center -> left");
                migrateItem(canvas4, canvas3, p.target);
                return;
            }
        }
    };

    var onObjectMovingCase3 = function(p) {
        var viewport = p.target.canvas.calcViewportBoundaries();

        // canvas 3 to canvas 4
        if (p.target.canvas === canvas) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas, canvas4, p.target);
                return;
            }
        }

        // canvas 4 to canvas 3
        if (p.target.canvas === canvas4) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas4, canvas, p.target);
                return;
            }
        }

        // canvas 3 to canvas 2
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas2, p.target);
                return;
            }
        }

        // canvas 2 to canvas 3
        if (p.target.canvas === canvas2) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas2, canvas, p.target);
                return;
            }
        }

        // canvas 3 to canvas 1
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas1, p.target);
                return;
            }
        }

        // canvas 1 to canvas 3
        if (p.target.canvas === canvas1) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas1, canvas, p.target);
                return;
            }
        }

        // canvas 1 to canvas 2
        if (p.target.canvas === canvas1) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas1, canvas2, p.target);
                return;
            }
        }

        // canvas 2 to canvas 1
        if (p.target.canvas === canvas2) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas2, canvas1, p.target);
                return;
            }
        }
    };

    var onObjectMovingCase4 = function(p) {
        var viewport = p.target.canvas.calcViewportBoundaries();

        // canvas 4 to canvas 3
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas3, p.target);
                return;
            }
        }

        // canvas 3 to canvas 4
        if (p.target.canvas === canvas3) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas3, canvas, p.target);
                return;
            }
        }

        // canvas 3 to canvas 2
        if (p.target.canvas === canvas3) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas3, canvas2, p.target);
                return;
            }
        }

        // canvas 2 to canvas 3
        if (p.target.canvas === canvas2) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas2, canvas3, p.target);
                return;
            }
        }

        // canvas 4 to canvas 2
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas2, p.target);
                return;
            }
        }

        // canvas 2 to canvas 4
        if (p.target.canvas === canvas2) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas2, canvas, p.target);
                return;
            }
        }

        // canvas 4 to canvas 1
        if (p.target.canvas === canvas) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas, canvas1, p.target);
                return;
            }
        }

        // canvas 1 to canvas 4
        if (p.target.canvas === canvas1) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas1, canvas, p.target);
                return;
            }
        }

        // canvas 2 to canvas 1
        if (p.target.canvas === canvas2) {
            if (p.target.left < viewport.tl.x) {
                migrateItem(canvas2, canvas1, p.target);
                return;
            }
        }

        // canvas 1 to canvas 2
        if (p.target.canvas === canvas1) {
            if (p.target.left > viewport.br.x) {
                migrateItem(canvas1, canvas2, p.target);
                return;
            }
        }
    };

    // switch case to determine in which canvas we are inserting
    // accordigly apply the cases
    switch ($("#selectCanvas").val()) {
        case 'canvasArea1':
            console.log('case 1');
            var canvas2 = new fabric.Canvas('canvasArea2');
            canvas2.renderAll();

            var canvas3 = new fabric.Canvas('canvasArea3');
            canvas3.renderAll();

            var canvas4 = new fabric.Canvas('canvasArea4');
            canvas4.renderAll();

            canvas.on("object:moving", onObjectMovingCase1);
            canvas2.on("object:moving", onObjectMovingCase1);
            canvas3.on("object:moving", onObjectMovingCase1);
            canvas4.on("object:moving", onObjectMovingCase1);
            break
        case 'canvasArea2':
            console.log('case 2');
            var canvas1 = new fabric.Canvas('canvasArea1');
            canvas1.renderAll();

            var canvas3 = new fabric.Canvas('canvasArea3');
            canvas3.renderAll();

            var canvas4 = new fabric.Canvas('canvasArea4');
            canvas4.renderAll();

            canvas.on("object:moving", onObjectMovingCase2);
            canvas1.on("object:moving", onObjectMovingCase2);
            canvas3.on("object:moving", onObjectMovingCase2);
            canvas4.on("object:moving", onObjectMovingCase2);
            break
        case 'canvasArea3':
            console.log('case 3');
            var canvas1 = new fabric.Canvas('canvasArea1');
            canvas1.renderAll();

            var canvas2 = new fabric.Canvas('canvasArea2');
            canvas2.renderAll();

            var canvas4 = new fabric.Canvas('canvasArea4');
            canvas4.renderAll();

            canvas.on("object:moving", onObjectMovingCase3);
            canvas1.on("object:moving", onObjectMovingCase3);
            canvas2.on("object:moving", onObjectMovingCase3);
            canvas4.on("object:moving", onObjectMovingCase3);
            break
        case 'canvasArea4':
            console.log('case 4');
            var canvas1 = new fabric.Canvas('canvasArea1');
            canvas1.renderAll();

            var canvas2 = new fabric.Canvas('canvasArea2');
            canvas2.renderAll();

            var canvas3 = new fabric.Canvas('canvasArea3');
            canvas3.renderAll();

            canvas.on("object:moving", onObjectMovingCase4);
            canvas1.on("object:moving", onObjectMovingCase4);
            canvas2.on("object:moving", onObjectMovingCase4);
            canvas3.on("object:moving", onObjectMovingCase4);
            break
    }
}